// CST2550 Coursework 1 Musa.cpp
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
class LoadSave
{
private:

	string CurrentLine;
public:
	string FilePath = "events.txt";
	vector<string> DataVector;
	void Load()
	{
		ifstream EventFile;
		EventFile.open(FilePath);
		while (getline(EventFile, CurrentLine))
		{
			DataVector.push_back(CurrentLine);

		}
		EventFile.close();

	}
};
class AllEvents : public LoadSave
{
public:
	vector<string> EventSort;
	vector<string> EventsAllSorted;
	vector<string> EventDetailsToSave;
	void SortEvents()
	{
		for (int i = 0; i < DataVector.size(); i++) {
			size_t pos = 0;
			while ((pos = DataVector[i].find(":")) != std::string::npos) {
				EventSort.push_back(DataVector[i].substr(0, pos));
				DataVector[i].erase(0, pos + 1);
			}
			string EventType;
			if (EventSort[1] == "LM") { //This if statement with 3 checks will assign A value to the string "EventType"
				EventType = "Live Music";
				EventsAllSorted.push_back("\n Event ID: " + to_string(i) + "\n Name of Event: " + EventSort[0] + "\n Event Type: " + EventType + "\n Seats remaining: " + EventSort[2] + "\n -------------------------");
				EventDetailsToSave.push_back(to_string(i) + ":" + EventSort[0] + ":" + EventSort[1] + ":" + EventSort[2] + ":");
			}
			else if (EventSort[1] == "SUC") {
				EventType = "Stand up comedy";
				EventsAllSorted.push_back("\n Event ID: " + to_string(i) + "\n Name of Event: " + EventSort[0] + "\n Event Type: " + EventType + "\n Seats remaining: " + EventSort[2] + "\n Seats already taken: "+ EventSort[3] + "\n------------------------ - ");
				EventDetailsToSave.push_back(to_string(i) + ":" + EventSort[0] + ":" + EventSort[1] + ":" + EventSort[2] + ":" + EventSort[3] + ":");
			}
			else {
				EventType = "Film";
				EventsAllSorted.push_back("\n Event ID: " + to_string(i) + "\n Name of Event: " + EventSort[0] + "\n Event Type: " + EventType + "\n Seats remaining: " + EventSort[2] + "\n 2D or 3D: " + EventSort[3] + "\n -------------------------");
				EventDetailsToSave.push_back(to_string(i) + ":" + EventSort[0] + ":" + EventSort[1] + ":" + EventSort[2] + ":" + EventSort[3] + ":");
			}
			EventSort.clear();
			cout << EventsAllSorted[i];
		}
	}
	void ViewGivenEvent(int SelectedID) {
		for (int i = 0; i < EventsAllSorted.size(); i++) {
			if (SelectedID == i) {
				cout << EventsAllSorted[i];
			}
		}
	}
};
class Bookings : public AllEvents
{
public:
	vector<string> EventsUpdated;
	void CreateBooking(int SelectedID, int ChosenSeat = 1) {
		ofstream OutputFile(FilePath);
		if (OutputFile.is_open()) {
			for (int i = 0; i < EventDetailsToSave.size(); i++) {
				if (SelectedID == i) {
					EventsUpdated.clear();
					size_t pos = 0;
					while ((pos = EventDetailsToSave[i].find(":")) != std::string::npos) {
						EventsUpdated.push_back(EventDetailsToSave[i].substr(0, pos));
						EventDetailsToSave[i].erase(0, pos + 1);
					}
					if (EventsUpdated[2] == "LM") { //This if statement with 3 checks will assign A value to the string "EventType"
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2];
						int SeatsUpdate = stoi(EventsUpdated[3]);
						SeatsUpdate--;
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + to_string(SeatsUpdate) + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}
					else if (EventsUpdated[2] == "SUC") {
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], TakenSeats = EventsUpdated[4];
						int SeatsUpdate = stoi(EventsUpdated[3]);
						vector<string> SeatsTakenVector;
						bool SeatAvailable = true;
							while ((pos = EventsUpdated[4].find("-")) != std::string::npos) {
								SeatsTakenVector.push_back(EventsUpdated[4].substr(0, pos));
								EventsUpdated[4].erase(0, pos + 1);
							}					
							for (int t = 1; t < SeatsTakenVector.size(); t++) {
								
								if (SeatsTakenVector[t] == to_string(ChosenSeat)) {
									cout << "Seat Already Taken";
									SeatAvailable = false;
									EventsUpdated.clear();
									EventsUpdated.push_back(EvName + ":" + EvType + ":" + to_string(SeatsUpdate) + ":" + TakenSeats + ":");
									OutputFile << EventsUpdated[0] + "\n";
									break;
								}
							}
							while (SeatAvailable == true) {
								SeatsUpdate--;
								EventsUpdated.clear();
								EventsUpdated.push_back(EvName + ":" + EvType + ":" + to_string(SeatsUpdate) + ":" + TakenSeats + "-" + to_string(ChosenSeat) + "-:");
								OutputFile << EventsUpdated[0] + "\n";
								SeatAvailable = false;
							}
					}
					else {
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], Ev2D3D = EventsUpdated[4];
						int SeatsUpdate = stoi(EventsUpdated[3]);
						SeatsUpdate--;
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + to_string(SeatsUpdate) + ":" + Ev2D3D + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}
				}
				else {
					size_t pos = 0;
					EventsUpdated.clear();
					while ((pos = EventDetailsToSave[i].find(":")) != std::string::npos) {
						EventsUpdated.push_back(EventDetailsToSave[i].substr(0, pos));
						EventDetailsToSave[i].erase(0, pos + 1);
					}
					//Code for writing rest of events back to text file. 
					if (EventsUpdated[2] == "LM") { //if statement with 3 checks
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], EvSeat = EventsUpdated[3];
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + EvSeat + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}
					else if (EventsUpdated[2] == "SUC") {
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], EvSeat = EventsUpdated[3], TakenSeats = EventsUpdated[4];
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + EvSeat + ":" + TakenSeats + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}
					else {
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], EvSeat = EventsUpdated[3], Ev2D3D = EventsUpdated[4];
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + EvSeat + ":" + Ev2D3D + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}

				}
			}
		}
	}
	void CancelBooking(int SelectedID, int ChosenSeat = 1) {
		ofstream OutputFile(FilePath);
		if (OutputFile.is_open()) {
			for (int i = 0; i < EventDetailsToSave.size(); i++) {
				if (SelectedID == i) {
					EventsUpdated.clear();
					size_t pos = 0;
					while ((pos = EventDetailsToSave[i].find(":")) != std::string::npos) {
						EventsUpdated.push_back(EventDetailsToSave[i].substr(0, pos));
						EventDetailsToSave[i].erase(0, pos + 1);
					}
					if (EventsUpdated[2] == "LM") { //This if statement with 3 checks will assign A value to the string "EventType"
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2];
						int SeatsUpdate = stoi(EventsUpdated[3]);
						SeatsUpdate++;
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + to_string(SeatsUpdate) + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}
					else if (EventsUpdated[2] == "SUC") {
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], TakenSeats = "";
						int SeatsUpdate = stoi(EventsUpdated[3]);
						vector<string> SeatsTakenVector;
						while ((pos = EventsUpdated[4].find("-")) != std::string::npos) {
							SeatsTakenVector.push_back(EventsUpdated[4].substr(0, pos));
							EventsUpdated[4].erase(0, pos + 1);
						}
						for (int t = 0; t < SeatsTakenVector.size(); t++) {

							if (SeatsTakenVector[t] == to_string(ChosenSeat)) {
								SeatsTakenVector[t] = "";
								SeatsUpdate++;
								for (int g = 0; g < SeatsTakenVector.size(); g++) {
									TakenSeats = TakenSeats + SeatsTakenVector[g] + "-";
								}
								cout << "Booked Seat Removed";
								EventsUpdated.clear();
								EventsUpdated.push_back(EvName + ":" + EvType + ":" + to_string(SeatsUpdate) + ":" + TakenSeats + ":");
								OutputFile << EventsUpdated[0] + "\n";
								break;
							}
						}
					}
					else {
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], Ev2D3D = EventsUpdated[4];
						int SeatsUpdate = stoi(EventsUpdated[3]);
						SeatsUpdate++;
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + to_string(SeatsUpdate) + ":" + Ev2D3D + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}
				}
				else {
					size_t pos = 0;
					EventsUpdated.clear();
					while ((pos = EventDetailsToSave[i].find(":")) != std::string::npos) {
						EventsUpdated.push_back(EventDetailsToSave[i].substr(0, pos));
						EventDetailsToSave[i].erase(0, pos + 1);
					}
					//Code for writing rest of events back to text file. 
					if (EventsUpdated[2] == "LM") { //if statement with 3 checks
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], EvSeat = EventsUpdated[3];
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + EvSeat + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}
					else if (EventsUpdated[2] == "SUC") {
						cout << EventsUpdated[1];
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], EvSeat = EventsUpdated[3], TakenSeats = EventsUpdated[4];
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + EvSeat + ":" + TakenSeats + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}
					else {
						string EvName = EventsUpdated[1], EvType = EventsUpdated[2], EvSeat = EventsUpdated[3], Ev2D3D = EventsUpdated[4];
						EventsUpdated.clear();
						EventsUpdated.push_back(EvName + ":" + EvType + ":" + EvSeat + ":" + Ev2D3D + ":");
						OutputFile << EventsUpdated[0] + "\n";
					}

				}
			}
		}
	}
};

int main()
{
	int MenuSelection;

	Bookings BeginSort;
	do
	{
	//BeginSort.ViewGivenEvent(1);
	BeginSort.Load();
	BeginSort.SortEvents();
		// Menu
		cout << "\n\t\t\t ----------------------------------";
		cout << "\n\t\t\t    Theatre Ticket Booking System";
		cout << "\n\t\t\t ----------------------------------";
		cout << "\n\t\t\t\t      Welcome!";  // Menu for the user
		cout << "\n\n\t\t\t\t <0> List events";
		cout << "\n\t\t\t\t <1> View Given Event";
		cout << "\n\t\t\t\t <2> Make Event Booking";
		cout << "\n\t\t\t\t <3> Cancel Event Booking";
		cout << "\n\t\t\t\t <4> Exit \n\n";
		cout << "\t\t\t\t   Select an option :"
			<< "\t";
		cin >> MenuSelection;
		switch (MenuSelection)
		{
		case 0:
			BeginSort.SortEvents();
			cout << "\n Do you want to choose another option(y/n)";
			cin >> MenuSelection;
			break;
		case 1:
			cout << "\t\t\t\t   Enter Event ID: ";
			cin >> MenuSelection;
			BeginSort.ViewGivenEvent(MenuSelection);
			cout << "\n Do you want to choose another option(y/n)";
			cin >> MenuSelection;
			break;
		case 2:
			cout << "\t\t\t\t   Enter Event ID: ";
			cin >> MenuSelection;
			BeginSort.CreateBooking(MenuSelection);
			cout << "\n Do you want to choose another option(y/n)";
			cin >> MenuSelection;
			break;
		case 3:
			cout << "\t\t\t\t   Enter Event ID: ";
			cin >> MenuSelection;
			BeginSort.CancelBooking(MenuSelection);
			cout << "\n Do you want to choose another option(y/n)";
			cin >> MenuSelection;
			break;
		case 4:
			exit(0);
			break;
		}
	} while (MenuSelection == 'y');
};
